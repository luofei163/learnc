
#include "Point.h"
struct Point
{
	int x;
	int y;
};

point *new_point()
{
	point *point_ = (point *)malloc(sizeof(point));
	return point_;
}

void free_point(point *point_)
{
	if (point_ == NULL)
		return;
	free(point_);
}

void set_x(point *p, int x1)
{
	p->x = x1;
}

int get_x(point *p)
{
	return p->x;
}