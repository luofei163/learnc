#ifndef _Point_H
#define _Point_H
#include <stdio.h>
#include <stdlib.h>
struct Point;
typedef struct Point point;

point *new_point();
void free_point(point *point_);
void set_x(point *p, int x1);
int get_x(point *p);
#endif